#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 

void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 

struct List* SortedIntersect(struct List* a, struct List* b){
    struct List dummy;
    struct List* tail = &dummy;
    dummy.next = NULL;
    
    while (a!=NULL && b!=NULL) {
        if (a->data == b->data) { 
            push(&tail->next, a->data);
            tail = tail->next;
            a=a->next;
            b=b->next;
        }
        else if (a->data < b->data) { 
            a=a->next;
        }
        else {
            b=b->next;
        }
    }
    
    return(dummy.next);
}


  
int main() 
{ 
    struct List* a = NULL;
    struct List* b = NULL;
    struct List* head;
    
    push(&a, 3); 
    push(&a, 2); 
    push(&a, 1);  
    push(&b, 6); 
    push(&b, 3);
    push(&b, 2);
    head = SortedIntersect(a,b);
    printf("The sorted Merge are ");
    
    while (head != NULL) { 
        printf("%d",head->data);
        printf(" ");
        head = head->next; 
    } 
   
} 