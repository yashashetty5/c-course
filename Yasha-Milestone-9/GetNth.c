#include <stdio.h> 
#include <stdlib.h> 
  
struct List { 
    int data; 
    struct List* next; 
}; 
  
void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = (struct List*)malloc(sizeof(struct List)); 
    new_node->data = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref) = new_node; 
} 
  
int GetNth(struct List* head, int index) 
{ 
    struct List* current = head; 
    int count = 0; 
    while (current != NULL) { 
        if (count == index) return(current->data);
            count++; 
        current = current->next; 
    } 
    return 0; 
} 
  
int main() 
{ 
    struct List* head = NULL; 
    push(&head, 5); 
    push(&head, 2); 
    push(&head, 1); 
    push(&head, 3); 
    push(&head, 5);
    push(&head, 5);
  
    printf("The data at index 2 is %d", GetNth(head, 2)); 
    return 0; 
} 
