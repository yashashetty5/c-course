#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
};


static void Reverse(struct List** head_ref){
    struct List* result = NULL;
    struct List* current = *head_ref;
    struct List* next;
    
    while(current!=NULL){
        next = current->next;
        current->next = result;
        result = current;
        current = next;
    }
    
    
    printf("Reversed List- ")
    *head_ref = result;
    while (result != NULL) { 
        printf("%d", result -> data);
        printf(" ");
        result = result->next; 
    }
    
}

void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 

  
int main() 
{ 
 
    struct List* head = NULL;
    
    push(&head, 1); 
    push(&head, 2); 
    push(&head, 3);  
    push(&head, 4); 
    push(&head, 5);
    push(&head, 6);
    
    Reverse(&head);


   
} 