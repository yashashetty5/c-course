#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int comp(const void *p1, const void *p2)
{
    return (*(int *)p1 - *(int *)p2);
}

int strComp(const void *p1, const void *p2)
{
    return strcmp(*(const char **)p1, *(const char **)p2);
}

void intSort()
{
    int n;
    printf("Enter the length of the array\n");
    scanf("%d", &n);

    int values[n];

    printf("\nEnter the array type\n");
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &values[i]);
    }

    qsort(values, n, sizeof(int), comp);

    printf("\nThe sorted array is: \n");
    for (int i = 0; i < n; i++)
    {
        printf("%d ", values[i]);
    }
}

void floatSort()
{
    int n;
    printf("Enter the length of the array\n");
    scanf("%d", &n);

    float values[n];

    printf("\nEnter the array type\n");
    for (int i = 0; i < n; i++)
    {
        scanf("%f", &values[i]);
    }

    qsort(values, n, sizeof(float), comp);

    printf("\nThe sorted array is: \n");
    for (int i = 0; i < n; i++)
    {
        printf("%f ", values[i]);
    }
}

void doubleSort()
{
    int n;
    printf("Enter the length of the array\n");
    scanf("%d", &n);

    double values[n];

    printf("\nEnter the array type\n");
    for (int i = 0; i < n; i++)
    {
        scanf("%lf", &values[i]);
    }

    qsort(values, n, sizeof(double), comp);

    printf("\nThe sorted array is: \n");
    for (int i = 0; i < n; i++)
    {
        printf("%lf ", values[i]);
    }
}

void stringSort()
{
    int n;
    printf("Enter the length of the array\n");
    scanf("%d", &n);

    char values[100][n];

    printf("\nEnter the array type\n");
    for (int i = 0; i < n; i++)
    {
        fgets(values[i], sizeof values, stdin);
    }

    qsort(values, n, sizeof(char *), strComp);

    printf("\nThe sorted array is: \n");
    for (int i = 0; i < n; i++)
    {
        printf("%s ", values[i]);
    }
}

int main()
{
    void (*fun_ptr_arr[])(int, int, int, int) = {intSort, floatSort, doubleSort, stringSort};

    int choice;

    printf("Enter your choice:\n1. Integer\n2. Float\n3. Double\n4. String\n");
    scanf("%d", &choice);

    switch (choice)
    {
    case 1:
        intSort();
        break;
    case 2:
        floatSort();
        break;
    case 3:
        doubleSort();
        break;
    case 4:
        stringSort();
        break;
    default:
        printf('nope');
        break;
    }

    return 0;
}